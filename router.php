<?php
    require_once("controller.php");
    require_once("RouterClass.php");

    // CONSTANTES PARA RUTEO
    define("BASE_URL", 'http://'.$_SERVER["SERVER_NAME"].':'.$_SERVER["SERVER_PORT"].dirname($_SERVER["PHP_SELF"]).'/');
    // M_PI = 3.1415...

    $r = new Router();

    
    // rutas
    $r->addRoute("libros", "GET", "LibrosController", "getAll");
    $r->addRoute("libro/:ID", "GET", "LibrosController", "get");

    //$r->addRoute("delete/:ID", "GET", "TasksController", "BorrarLaTaskQueVienePorParametro");

    //Ruta por defecto.
    $r->setDefaultRoute("LibrosController", "getAll");

    //run
    $r->route($_GET['action'], $_SERVER['REQUEST_METHOD']);

    /*
    $action = "libros";
    if(isset($_GET['action']) && $_GET['action'] != ""){
        $action = $_GET['action']; // libro/2
    }
    $params = explode("/", $action); // ["libro", "2"]
    $action = array_shift($params); // $action = "libro" , $params = ["2"]

    require_once("controller.php");
    $contoller = new LibrosController();

    switch ($action) {
        case 'libros':
            $contoller->getAll();
            break;
        case 'libro':
            $contoller->get($params);
        default:
            # code...
            break;
    }*/


?>