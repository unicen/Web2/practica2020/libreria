
Base de datos: 
    Libros (id, nombre, descripción, id_genero)
            1   Guerra y Paz, Libro de tolstoi sobre cosas, 2
            2   El principito, Trata de un extraterrestre que tenía una flor.            
            4   Guerra y Paz, Libro que plagió Nico.


    Genero (id, nombre, descripción)




Vistas:
   >Mostrar todos los Libros (Lista)
   >Mostrar un libro con su descripción
    ABM de un libro
    Mostrar categrorías

Router:
    /libros                 -> lista
    /libro/[id]             -> un libro
    /libro/                 -> agregar
    /categrorías            -> lista de categrorías
    /categoria/[id]         -> una categoria
    /libros?categoria=?     -> lista


    
Implementar:
    * Base de datos (phpmyadmin)
    * LibroModel 
    * CategoriaModel
    * LibroView/LibroControler/Router (Incremental)
      * getAll()
      * get($id)
      * add($params)
      * delete($id)
      * edit($id, $params)






    <?php
     $model = new MiModel();
     $all = model->$getAll();
     print_r($all);