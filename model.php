<?php

    class LibrosModel{

        private $db;

        function __construct(){
            $this->db = new PDO('mysql:host=localhost;'.'dbname=Libreria;charset=utf8', 'user', 'user');
        }

        function getAll(){
            // Construllo una sentencia
            // sentencia es un objeto que sabe pedir todos los libros a la base de datos
            $sentencia = $this->db->prepare("SELECT id,nombre FROM Libro");

            // ejecuto la sentencia
            $sentencia->execute();

            // Traer a una variable todos los registros encontrados
            $libros = $sentencia->fetchAll(PDO::FETCH_OBJ);
            // [ (id, nombre), (id, nombre), (id, nombre) ]

            // $libros[1]->nombre 
            return $libros;
        }

        function get($id){
            // Construllo una sentencia
            // sentencia es un objeto que sabe pedir un libro especifico a la base de datos
            $sentencia = $this->db->prepare("SELECT id,nombre,descripcion FROM Libro WHERE id = ?");

            // ejecuto la sentencia
            $sentencia->execute([$id]);

            $libro = $sentencia->fetch(PDO::FETCH_OBJ);
            // (id, nombre, descripcion)

            return $libro;
        }
    }
?>