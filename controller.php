<?php
    require_once("model.php");
    require_once("view.php");

    class LibrosController{
        private $model;
        private $view;

        function __construct(){
            $this->model = new LibrosModel();
            $this->view = new LibrosView();
        }

        function getAll(){
            # Pedirle los libros a la base de datos
            $libros = $this->model->getAll();
            # Mostrar los libros en una lista
            $this->view->showAll($libros);
        }

        function get($params = null){
            $id = $params[':ID'];
            $libro = $this->model->get($id);
            $this->view->show($libro);
        }
    }
?>