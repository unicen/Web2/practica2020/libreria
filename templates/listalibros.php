<!-- mostrarLibros($libros) -->
<html>
    <head>
        <base href="<?=BASE_URL?>" />
    </head>
    <body>
        <ul>
            <?php foreach ($libros as $id => $libro): ?>
                <li><a href="libro/<?=$libro->id?>"> <?=$libro->nombre?> </a></li>
            <?php endforeach ?>
        </ul>
    </body>
</html>